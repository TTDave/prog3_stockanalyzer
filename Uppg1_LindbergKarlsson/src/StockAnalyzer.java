

// Modified JSONObject.java to use LinkedHashMap instead of HashMap to retain order.
public class StockAnalyzer {

	public static void main(String[] args) {

		StockAnalyzerView view = new StockAnalyzerView();
		StockAnalyzerModel model = new StockAnalyzerModel();
		
		StockAnalyzerController controller = new StockAnalyzerController(view, model);
		
		view.setVisible(true);
		
		
		// Lyckades inte lista ut hur vi skulle f� diagram att synas korrekt. Nu visar det... n�got... ibland.
		// Datumet uppdaterar (men �r i string-format). M�ste antagligen formatera om datumet till n�got annat format,
		// och kommer antagligen pr�va n�got annat �n JFreeChart, d� vi inte riktigt gillar (eller f�rst�r) hur den fungerar.

	}

}
