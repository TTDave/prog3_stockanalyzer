import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Arrays;

import javax.swing.JOptionPane;

import org.jfree.data.xy.XYSeries;
import org.json.*;

public class StockAnalyzerController {
	
	
// DECLARING VARIABLES	
	private StockAnalyzerView view;
	private StockAnalyzerModel model;
	String query_url;
	String modelOutput;
	String viewInput;
	
	public StockAnalyzerController(StockAnalyzerView view, StockAnalyzerModel model) {
		
		this.view = view;
		this.model = model;
		
// ADDING LISTENERS TO BUTTON/COMBOBOXES
		this.view.addButtonListener(new StockListener());
		this.view.addDropdownListener(new StockListener());
	}
	
	class StockListener implements ActionListener {

// ACTION IS PERFORMED WHEN BUTTON IS PRESSED OR WHEN DATA SERIES COMBOBOX IS UPDATED
		public void actionPerformed(ActionEvent e) {
			
		// CREATING STRING FOR 'MODULAR' URL
			String dataSeries = view.getDataSeries();
			String timeSeries = view.getTimeSeries();
			String timeInterval = view.getTimeInterval();
			String tickerSymbol = view.getTickerSymbol();
			String outputSize = view.getOutputSize();
			
			query_url = "https://www.alphavantage.co/query?function=" + timeSeries 
					+ "&symbol=" + tickerSymbol 
					+ "&interval=" + timeInterval 
					+ "&outputsize=" + outputSize
					+ "&apikey=DQNQR0SY0TWDIHBJ";
			
			
		// REQUEST THE JSON DATA FROM MODEL
			try {
				modelOutput = model.getJson(query_url);
			} catch (IOException ea) {
				ea.printStackTrace();
			}
			
			
		// FORMATTING THE JSON DATA
		// NOTE: MODIFIED JSONObject.java FROM USING HASHMAP TO LINKED HASHMAP IN ORDER TO RETAIN THE ORDER OF CHILD OBJECTS
		// SEE JSONObject.java LINES 188-189
			
			JSONObject obj = new JSONObject(modelOutput);
			JSONObject objChild = null;
			
			String timeSeriesCheck = view.getTimeSeries();
			
		// DETERMINES WHAT STRING TO USE FOR obj.get METHOD	
		// DISABLES COMBOBOXES BASED ON SELECTION (I.E. NO TIME INTERVAL ON DAILY TIME SERIES, ETC)
			try {
				if ( timeSeriesCheck == "TIME_SERIES_INTRADAY") {
					objChild = (JSONObject)obj.get("Time Series ("+ view.getTimeInterval() + ")");
					view.timeIntervalDropdown.setEnabled(true);
					view.outputSizeDropdown.setEnabled(true);
				}
				
				else if ((timeSeriesCheck == "TIME_SERIES_DAILY") || (timeSeriesCheck == "TIME_SERIES_DAILY_ADJUSTED")) {
					objChild = (JSONObject)obj.get("Time Series (Daily)");	
					view.timeIntervalDropdown.setEnabled(false);
					view.outputSizeDropdown.setEnabled(true);
				}
				
				else if (timeSeriesCheck == "TIME_SERIES_WEEKLY") {
					objChild = (JSONObject)obj.get("Weekly Time Series");
					view.timeIntervalDropdown.setEnabled(false);
					view.outputSizeDropdown.setEnabled(false);
					
				}
				
				else if (timeSeriesCheck == "TIME_SERIES_WEEKLY_ADJUSTED") {
					objChild = (JSONObject)obj.get("Weekly Adjusted Time Series");
					view.timeIntervalDropdown.setEnabled(false);
					view.outputSizeDropdown.setEnabled(false);
				}
				
				else if (timeSeriesCheck == "TIME_SERIES_MONTHLY") {
					objChild = (JSONObject)obj.get("Monthly Time Series");
					view.timeIntervalDropdown.setEnabled(false);
					view.outputSizeDropdown.setEnabled(false);
				}
				
				else if (timeSeriesCheck == "TIME_SERIES_MONTHLY_ADJUSTED") {
					objChild = (JSONObject)obj.get("Monthly Adjusted Time Series");
					view.timeIntervalDropdown.setEnabled(false);
					view.outputSizeDropdown.setEnabled(false);
				}
				
	// SOMETIMES A NULL POINTER, JSON, OR SOME OTHER EXCEPTION OCCURS WHEN DATA IS REQUESTED TOO RAPIDLY
	// WAITING USUALLY SOLVES THE PROBLEM (MAY SOMETIMES HAVE TO SWITCH TIME SERIES). SEEMS LIKE API REQUEST GETS 'OVERLOADED' OR SOMETHING?
			} catch (Exception f) {
				JOptionPane.showMessageDialog(null, "Too many queries! Please wait a while before doing another query (or restart the program).", 
						"Error!", JOptionPane.ERROR_MESSAGE);
			}


	// CREATING JSONARRAYS FOR DATE NAMES (I.E. 10.03.2019 20.00.01) AND DATA (I.E. 117.6000)				
			JSONArray dateNames = objChild.names();
			JSONArray datesArray = objChild.toJSONArray(dateNames);
			
	// CREATING ARRAY OF DOUBLE FOR DATA
			double[] dataDoubleArray = new double[datesArray.length()];
			
	// CREATING STRING ARRAY FOR DATES IN ORDER TO GET THEM TO HISTOGRAM
			String[] dateStringArray = new String[datesArray.length()];
			
			viewInput = "=== Showing data for " + view.getDataSeries() + "=== \n";

	// GENERATING A LONG STRING BASED ON ARRAYS THAT EVENTUALLY SHOWS IN OUTPUT
	// ADDING JSONArray DATA TO REGULAR ARRAYS (FOR DRAWING CHART).
			for(int i = 0; i < datesArray.length(); i++) {
				String date = dateNames.get(i).toString();
				String data = datesArray.getJSONObject(i).getString(dataSeries);
				viewInput += "Date: " + date + " " + data + "\n";
				dateStringArray[i] = dateNames.get(i).toString();
				dataDoubleArray[i] = Double.parseDouble(datesArray.getJSONObject(i).getString(dataSeries));
			}
			

	// UPDATES TEXT FIELD 			
			view.setOutputData(viewInput);
			
	// UPDATES NON-FUNCTIONAL GRAPH
			view.updateData(dataDoubleArray);		
			view.updateDates(dateStringArray);
			view.render();
		}
	}
}
