import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.axis.SymbolAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.data.statistics.HistogramDataset;
import org.jfree.data.xy.DefaultXYDataset;

public class StockAnalyzerView extends JFrame implements ActionListener {



// DECLARING VARIABLES TO MAKE THEM ACCESSIBLE FOR METHODS	
	JComboBox dataSeriesDropdown, timeSeriesDropdown, tickerSymbolDropdown, timeIntervalDropdown, outputSizeDropdown;
	JButton query = new JButton("---- Do Query ----");
	JTextArea outputData = new JTextArea();
	
	JFreeChart chart;
	HistogramDataset ds = new HistogramDataset();
	XYBarRenderer renderer = new XYBarRenderer();

	XYPlot plot;
	ChartPanel cp;



	public StockAnalyzerView() {
		
// CREATING AND SETTING GUI PROPERTIES
		JFrame guiFrame = new JFrame();
		
		guiFrame.setTitle("Stock Analyzer");
		guiFrame.setSize(1250, 800);

		GridBagLayout gridBag = new GridBagLayout();
		GridBagConstraints constraints = new GridBagConstraints();
		
		JPanel panel = new JPanel();
		panel.setPreferredSize(new Dimension(1250, 800));
		panel.setLayout(gridBag);

		
// DECLARING ALL LABELS
		JLabel dataSeries, timeSeries, tickerSymbol, timeInterval, outputSize;		
		
// CREATING ARRAYS FOR JCOMBOBOX OPTIONS
		
		String[] dataSeriesArray = {"1. open", "2. high", "3. low", "4. close", "5. volume"};
		
		String[] timeSeriesArray = {"TIME_SERIES_INTRADAY",  
									"TIME_SERIES_DAILY", 
									"TIME_SERIES_DAILY_ADJUSTED", 
									"TIME_SERIES_WEEKLY", 
									"TIME_SERIES_WEEKLY_ADJUSTED", 
									"TIME_SERIES_MONTHLY", 
									"TIME_SERIES_MONTHLY_ADJUSTED"};
		
		String[] tickerSymbolArray = {"MSFT", "AAPL"};
		
		String[] intervalArray = {"1min", "5min", "15min", "30min", "60min"};
		
		String[] outputSizeArray = {"compact", "full"};
		
		
		
	
// LABELS
		
	// DATA SERIES LABEL
		dataSeries = new JLabel("Data Series");
		constraints.anchor = GridBagConstraints.WEST;
		constraints.weightx = 1.0;
		constraints.insets = new Insets(10,10,10,10);
		constraints.gridy = 0;
		constraints.gridx = 0;
		gridBag.setConstraints(dataSeries, constraints);
		panel.add(dataSeries);
				
	// TIME SERIES LABEL
		timeSeries = new JLabel("Time Series");
		constraints.anchor = GridBagConstraints.WEST;
		constraints.weightx = 1.0;
		constraints.insets = new Insets(10,10,10,10);
		constraints.gridy = 1;
		constraints.gridx = 0;
		gridBag.setConstraints(timeSeries, constraints);
		panel.add(timeSeries);
		
	// TICKER SYMBOL LABEL
		tickerSymbol = new JLabel("Ticker Symbol");
		constraints.anchor = GridBagConstraints.WEST;
		constraints.weightx = 1.0;
		constraints.insets = new Insets(10,10,10,10);
		constraints.gridy = 2;
		constraints.gridx = 0;
		gridBag.setConstraints(tickerSymbol, constraints);
		panel.add(tickerSymbol);
		
	// TIME INTERVAL LABEL
		timeInterval = new JLabel("Time Interval");
		constraints.anchor = GridBagConstraints.WEST;
		constraints.weightx = 1.0;
		constraints.insets = new Insets(10,10,10,10);
		constraints.gridy = 3;
		constraints.gridx = 0;
		gridBag.setConstraints(timeInterval, constraints);
		panel.add(timeInterval);
		
	// OUTPUT SIZE LABEL
		outputSize = new JLabel("Output Size");
		constraints.anchor = GridBagConstraints.WEST;
		constraints.weightx = 1.0;
		constraints.insets = new Insets(10,10,10,10);
		constraints.gridy = 4;
		constraints.gridx = 0;
		gridBag.setConstraints(outputSize, constraints);
		panel.add(outputSize);
		
		
		
// DROPDOWNS/COMBOBOXES
		
	// DATA SERIES COMBOBOX
		dataSeriesDropdown = new JComboBox(dataSeriesArray);
		dataSeriesDropdown.setSelectedIndex(0);
		constraints.anchor = GridBagConstraints.WEST;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.weightx = 5.0;
		constraints.insets = new Insets(10,10,10,10);
		constraints.gridy = 0;
		constraints.gridx = 1;
		gridBag.setConstraints(dataSeriesDropdown, constraints);
		panel.add(dataSeriesDropdown);
		
	// TIME SERIES COMBOBOX		
		timeSeriesDropdown = new JComboBox(timeSeriesArray);
		timeSeriesDropdown.setSelectedIndex(0);
		constraints.anchor = GridBagConstraints.WEST;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.weightx = 5.0;
		constraints.insets = new Insets(10,10,10,10);
		constraints.gridy = 1;
		constraints.gridx = 1;
		gridBag.setConstraints(timeSeriesDropdown, constraints);
		panel.add(timeSeriesDropdown);
		
	// TICKER SYMBOL COMBOBOX
		tickerSymbolDropdown = new JComboBox(tickerSymbolArray);
		tickerSymbolDropdown.setSelectedIndex(0);
		constraints.anchor = GridBagConstraints.WEST;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.weightx = 5.0;
		constraints.insets = new Insets(10,10,10,10);
		constraints.gridy = 2;
		constraints.gridx = 1;
		gridBag.setConstraints(tickerSymbolDropdown, constraints);
		panel.add(tickerSymbolDropdown);
		
	// TIME INTERVAL COMBOBOX	
		timeIntervalDropdown = new JComboBox(intervalArray);		
		timeIntervalDropdown.setSelectedIndex(0);
		constraints.anchor = GridBagConstraints.WEST;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.weightx = 5.0;
		constraints.insets = new Insets(10,10,10,10);
		constraints.gridy = 3;
		constraints.gridx = 1;
		gridBag.setConstraints(timeIntervalDropdown, constraints);
		panel.add(timeIntervalDropdown);
		
	// OUTPUT SIZE COMBOBOX		
		outputSizeDropdown = new JComboBox(outputSizeArray);
		outputSizeDropdown.setSelectedIndex(0);
		constraints.anchor = GridBagConstraints.WEST;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.weightx = 5.0;
		constraints.insets = new Insets(10,10,10,10);
		constraints.gridy = 4;
		constraints.gridx = 1;
		gridBag.setConstraints(outputSizeDropdown, constraints);
		panel.add(outputSizeDropdown);
				
		
		
// BUTTONS

	// QUERY BUTTON
		query.setPreferredSize(new Dimension(50, 50));
		constraints.anchor = GridBagConstraints.WEST;
		constraints.ipadx = 50;
		constraints.weightx = 5.0;
		constraints.insets = new Insets(10,10,10,10);
		constraints.gridy = 5;
		constraints.gridx = 1;
		gridBag.setConstraints(query, constraints);
		panel.add(query);
		query.addActionListener(this);

		
		
// TEXTFIELDS
		
	// OUTPUT TEXTFIELD
		JScrollPane scrollPane = new JScrollPane(outputData);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.weighty = 2.0;
		constraints.gridy = 6;
		constraints.gridx = 0;
		constraints.gridwidth  = 2;
		outputData.setEditable(false);
		
		gridBag.setConstraints(scrollPane, constraints);
		panel.add(scrollPane);
		
		
		
// HISTOGRAM
		
		// CREATING EMPTY HISTOGRAM		
//		JFreeChart chart = ChartFactory.createXYLineChart("Histogram", "Dates:", "Value", ds, PlotOrientation.VERTICAL, true, true, false);
		chart = ChartFactory.createHistogram("Histogram", "Time", "Value", ds, PlotOrientation.VERTICAL, false, false, false);
		plot = (XYPlot)chart.getPlot();
		plot.setRangeGridlinesVisible(false);
		plot.setDomainGridlinesVisible(false);
		plot.setOutlineVisible(false);
		plot.setRangeZeroBaselineVisible(false);
		plot.setDomainZeroBaselineVisible(false);
		plot.getDomainAxis().setTickLabelsVisible(false);
		plot.setBackgroundImageAlpha(0.0f);
		
		
		renderer.setDrawBarOutline(true);
		renderer.setShadowVisible(false);
		
		
		cp = new ChartPanel(chart);	
		constraints.anchor = GridBagConstraints.EAST;
		constraints.gridy = 0;
		constraints.gridx = 2;
		constraints.gridwidth  = 15;
		constraints.gridheight = 7;
		constraints.weightx = 20.0;
		gridBag.setConstraints(cp, constraints);
		panel.add(cp);
		
		
		
// FINALIZING FRAME
		
		guiFrame.setLocationRelativeTo(null);						// CENTERS WINDOW TO MIDDLE OF SCREEN
		guiFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	// TERMINATES PROGRAM WHEN WINDOW IS CLOSED
		guiFrame.add(panel);
		guiFrame.pack();
		guiFrame.setVisible(true);
		
		
	
	}


	private DefaultXYDataset createDataset() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	public void actionPerformed(ActionEvent e) {
		System.out.println("Action performed");	
		}	
	
	
// PUBLIC GETTER METHODS FOR COMBOBOX VALUES
	public String getDataSeries() {
		return String.valueOf(dataSeriesDropdown.getSelectedItem());
	}
	
	public String getTimeSeries() {
		return String.valueOf(timeSeriesDropdown.getSelectedItem());
	}
	
	public String getTickerSymbol() {
		return String.valueOf(tickerSymbolDropdown.getSelectedItem());
	}
	
	public String getTimeInterval() {
		return String.valueOf(timeIntervalDropdown.getSelectedItem());
	}
	
	public String getOutputSize() {
		return String.valueOf(outputSizeDropdown.getSelectedItem());
	}
	
	
// PUBLIC SETTER METHOD FOR ADDING DATA TO TEXTFIELD		
	public void setOutputData(String input) {
		outputData.append(input);
		outputData.append("\n");
	}
	

// PUBLIC SETTER METHOD FOR UPDATING HISTOGRAM X-AXIS (DATE/TIME)
	void updateDates(String[] datesArray) {
		SymbolAxis domainAxis = new SymbolAxis("Dates", datesArray);

	    domainAxis.setTickUnit(new NumberTickUnit(1));
	    domainAxis.setRange(0,datesArray.length);
	    plot.setDomainAxis(domainAxis);
		cp.repaint();
	}
	
// PUBLIC SETTER METHOD FOR UPDATING HISTOGRAM Y-AXIS (DATA/VALUES)
	
	void updateData(double[] dataArray) {
		ds.addSeries("Values", dataArray, 1);
		cp.repaint();
	}
	
// RENDER THE CHART
	void render() {
		chart.getXYPlot().setDataset(chart.getXYPlot().getDataset());
	}
	

	
// ADD LISTENER METHODS FOR QUERY BUTTON...
	void addButtonListener(ActionListener listenForButton) {
		query.addActionListener(listenForButton);
	}

// ...AND DATA SERIES COMBOBOX
	void addDropdownListener(ActionListener listenForSelection) {
		dataSeriesDropdown.addActionListener(listenForSelection);
	}
}
	

